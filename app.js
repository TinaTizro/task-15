var computerGuess;
var userGuessLog = [];
var attempts = 0;
var maxGuesses = 10;

function easyMode() {
    maxGuesses = 10;

}
function hardMode() {
    maxGuesses = 5;
}
function newGame() {
    window.location.reload();
}
function init() {
    computerGuess = Math.floor(Math.random() * 100 + 1);
    console.log(computerGuess);
    document.getElementById('newGameBtn').style.display = none;


}
function compareGuess() {
    var userGuess ="" + document.getElementById('inputBox').value;
    // console.log(userGuess);
    userGuessLog.push(userGuess);
    // console.log(userGuess);
    document.getElementById('guessLog').innerHTML = userGuessLog;

    attempts++; //Question
    document.getElementById('attempts').innerHTML = attempts;
    if (userGuessLog.length < maxGuesses) {
        if (userGuess > computerGuess) {
            document.getElementById('textOutput').innerHTML = 'Your guess is too high';
            document.getElementById('inputBox').value = "";
        } else if (userGuess < computerGuess) {
            document.getElementById('textOutput').innerHTML = 'Your guess is too low';
            document.getElementById('inputBox').value = "";
        } else {
            document.getElementById('textOutput').innerHTML = 'Congratulations! Your guess is correct';
        }
    }else {
        if (userGuess > computerGuess){
            document.getElementById('textOutput').innerHTML ='You lose!';
        }else if (userGuess < computerGuess){
            document.getElementById('textOutput').innerHTML = 'You lose!';
        }else {
            document.getElementById('textOutput'). innerHTML = 'Correct!';
        }
    }
}